package com.example.customprogressbar

import android.animation.TimeInterpolator
import android.animation.ValueAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import kotlin.math.min

class CircularProgressBar(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    private val DEFAULT_MAX_VALUE = 100
    private val DEFAULT_START_ANGLE = 270

    private var progressAnimator: ValueAnimator? = null

    private var rectF = RectF()
    private var backgroundPaint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
    }
    private var foregroundPaint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
    }

    var progress: Int = 0
        set(value) {
            field = if (progress <= progressMax) value else progressMax
            onProgressChangeListener?.invoke(progress)
            invalidate()
        }
    var progressMax: Int = DEFAULT_MAX_VALUE
        set(value) {
            field = if (field >= 0) value else DEFAULT_MAX_VALUE
            invalidate()
        }
    var progressBarWidth: Float = resources.getDimension(R.dimen.default_stroke_width)
        set(value) {
            field = value.dpToPx()
            foregroundPaint.strokeWidth = field
            requestLayout()
            invalidate()
        }
    var backgroundProgressBarWidth: Float = resources.getDimension(R.dimen.default_background_stroke_width)
        set(value) {
            field = value.dpToPx()
            backgroundPaint.strokeWidth = field
            requestLayout()
            invalidate()
        }
    var progressBarColor: Int = Color.BLACK
        set(value) {
            field = value
            manageColor()
            invalidate()
        }
    var backgroundProgressBarColor: Int = Color.GRAY
        set(value) {
            field = value
            manageBackgroundProgressBarColor()
            invalidate()
        }
    var startAngle: Int = DEFAULT_START_ANGLE
        set(value) {
            var angle = value + DEFAULT_START_ANGLE
            while (angle > 360) {
                angle -= 360
            }
            field = if (angle < 0) 0 else if (angle > 360) 360 else angle
            invalidate()
        }
    var onProgressChangeListener: ((Int) -> Unit)? = null

    init {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.CircularProgressBar, 0, 0)

        progress = attributes.getInt(R.styleable.CircularProgressBar_progress, progress)
        progressMax = attributes.getInt(R.styleable.CircularProgressBar_progress_max, progressMax)

        // StrokeWidth
        progressBarWidth = attributes.getDimension(R.styleable.CircularProgressBar_progressbar_width, progressBarWidth).pxToDp()
        backgroundProgressBarWidth = attributes.getDimension(R.styleable.CircularProgressBar_background_progressbar_width, backgroundProgressBarWidth).pxToDp()

        // Color
        progressBarColor = attributes.getInt(R.styleable.CircularProgressBar_progressbar_color, progressBarColor)
        backgroundProgressBarColor = attributes.getInt(R.styleable.CircularProgressBar_background_progressbar_color, backgroundProgressBarColor)

        // Angle
        startAngle = attributes.getInt(R.styleable.CircularProgressBar_start_angle, 0)

        attributes.recycle()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        progressAnimator?.cancel()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        manageColor()
        manageBackgroundProgressBarColor()
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawOval(rectF, backgroundPaint)
        val realProgress = progress * DEFAULT_MAX_VALUE / progressMax
        val angle = 360 * realProgress / 100

        canvas.drawArc(rectF, startAngle.toFloat(), angle.toFloat(), false, foregroundPaint)
    }

    override fun setBackgroundColor(backgroundColor: Int) {
        backgroundProgressBarColor = backgroundColor
    }

    private fun manageColor() {
        foregroundPaint.shader = createLinearGradient(progressBarColor,progressBarColor)
    }

    private fun manageBackgroundProgressBarColor() {
        backgroundPaint.shader = createLinearGradient(backgroundProgressBarColor, backgroundProgressBarColor)
    }

    private fun createLinearGradient(startColor: Int, endColor: Int): LinearGradient {
        return LinearGradient(0f, 0f, 0f, 0f, startColor, endColor, Shader.TileMode.CLAMP)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val min = min(width, height)
        setMeasuredDimension(min, min)
        val highStroke = if (progressBarWidth > backgroundProgressBarWidth) progressBarWidth else backgroundProgressBarWidth
        rectF.set(0 + highStroke / 2, 0 + highStroke / 2, min - highStroke / 2, min - highStroke / 2)
    }

    fun setProgressWithAnimation(progress: Int,
                                 duration: Long? = null,
                                 interpolator: TimeInterpolator? = null,
                                 startDelay: Long? = null) {
        progressAnimator?.cancel()
        progressAnimator = ValueAnimator.ofInt(this.progress, progress)
        duration?.also { progressAnimator?.duration = it }
        interpolator?.also { progressAnimator?.interpolator = it }
        startDelay?.also { progressAnimator?.startDelay = it }
        progressAnimator?.addUpdateListener { animation ->
            (animation.animatedValue as? Int)?.also { value ->
                this.progress = value
            }
        }
        progressAnimator?.start()
    }

    private fun Float.dpToPx(): Float = this * Resources.getSystem().displayMetrics.density

    private fun Float.pxToDp(): Float = this / Resources.getSystem().displayMetrics.density

}
