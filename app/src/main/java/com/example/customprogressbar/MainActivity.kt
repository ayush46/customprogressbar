package com.example.customprogressbar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var circleProgressBar: CircularProgressBar? = null
    private var editText : EditText? = null
    private var btn : Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        circleProgressBar = findViewById(R.id.circularProgressBar)
        editText = findViewById(R.id.et)
        btn = findViewById(R.id.btn)

        btn?.setOnClickListener {
            circularProgressBar.setProgressWithAnimation(editText?.text.toString().toInt(), 800)
        }

    }

}
